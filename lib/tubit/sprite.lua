local TubitSprite = {}
local TubitSprite_mt = { __index = TubitSprite }

function TubitSprite.new(sheet, x, y, w, h)
	assert(h ~= nil, "new palette requires a table of colors")

	return setmetatable({
		x = x,
		y = y,
		w = w,
		h = h,
		offsetx = 0,
		offsety = 0,
		fliph = false,
		fliph = false,
		sheet = sheet,
		spr = love.graphics.newQuad(x, y, w, h, sheet:getDimensions())
	}, TubitSprite_mt)
end

function TubitSprite:clone()
	return TubitSprite.new(self.sheet, self.x, self.y, self.w, self.h)
end

function TubitSprite:getWidth() return self.w end
function TubitSprite:getHeight() return self.h end

function TubitSprite:flipH(b)
	self.fliph = b
	self.offsetx = (b and self.offsetx == 0) and self.w or 0
	return self
end

function TubitSprite:flipV(b)
	self.flipv = b
	self.offsety = (b and self.offsety == 0) and self.h or 0
	return self
end

function TubitSprite:draw(x, y)
	love.graphics.setColor(1, 1, 1)
	love.graphics.draw(
		self.sheet,
		self.spr,
		x, y, 0,
		self.fliph and -1 or 1,
		self.flipv and -1 or 1,
		self.offsetx,
		self.offsety
	)
end

return TubitSprite