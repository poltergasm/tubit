-- Font credits: https://www.dafont.com/early-gameboy.font

local tpath = ...
local Tubit = {}
local Tubit_mt = { __index = Tubit }

-- tile size
local ts = 16

local Palette = require(tpath .. ".palette")
local Sprite = require(tpath .. ".sprite")
local Animation = require(tpath .. ".animation")
local TLfres = require(tpath .. ".ext.tlfres")
local Baton = require(tpath .. ".ext.baton")

-- mostly an internal function to cut the image up into quads
function Tubit.generate_quads(img)
	local w,h = img:getDimensions()
    local sprite_quads = {}
    for y=1, w/ts do
        for x=1, h/ts do
            table.insert(sprite_quads,
                love.graphics.newQuad((x-1)*ts, (y-1)*ts, ts, ts, w, h))
        end
    end

    return sprite_quads
end

function Tubit.loadJoystick(path)
	love.joystick.loadGamepadMappings(path .. "/res/gamecontrollerdb.txt")
	local joystick = love.joystick.getJoysticks()[1]

	Tubit.input = Baton.new {
	    controls = {
	        ["up"]      = {'key:up', 'button:dpup'},
	        ["down"]    = {'key:down', 'button:dpdown'},
	        ["left"]    = {'key:left', 'button:dpleft'},
	        ["right"]   = {'key:right', 'button:dpright'},
	        ["a"]   = {'key:x', 'button:a'},
	        ["b"]   = {'key:z', 'button:x'},
	        ["select"]  = {'key:lshift', 'button:back'},
	        ["start"]   = {'key:return', 'button:start'}
	    },
	    joystick = joystick
	}
end

function Tubit.new(path, filter)
	assert(path ~= nil, "new requires the path to a spritesheet")

	-- load the image data
	local data = love.image.newImageData(path)
	assert(data, "Your path to newSpritesheet does not appear to be a valid image")

	-- if they want the default filter, then set it
	if filter then love.graphics.setDefaultFilter("nearest", "nearest") end

	local img = love.graphics.newImage(data)
	local pth = tpath:gsub("%.", "/")

	Tubit.loadJoystick(pth)

	local font = love.graphics.newFont(pth .. "/res/Early_GameBoy.ttf", 8)
	love.graphics.setFont(font)
	return setmetatable({
		data = data,
		image = img,
		quads = Tubit.generate_quads(img),
		font = font,
		pal = Palette.new(Palette.getDefaultPalette(), pth) }, Tubit_mt)
end

-- add a new table to the list
function Tubit:addPal(tbl)
	assert(tbl ~= nil and type(tbl) == "table", "Adding a palette must be a table")

	for col=1, 4 do
		tbl[col] = Palette.get_color(tbl[col])
	end

	table.insert(self.pal.tbl, tbl)
end

-- very basic print function. completely optional!
-- string, x, y, color number
function Tubit:print(str, x, y, c)
	local r, g, b, a = love.graphics.getColor()
	love.graphics.setColor(self:getColor(c))
	love.graphics.setFont(self.font)
	love.graphics.print(str, x, y)
	love.graphics.setColor(r, g, b, a)
end

-- swaps the palette and updates the image
function Tubit:setPal(n)
	assert(self.pal.tbl[n] ~= nil, "Palette (" .. n .. ") does not exist")

	self.prev_pal = self.pal.tbl[self.pal.current_pal]

	-- no. I'm not explaining this
	self.data:mapPixel(function(x, y, r, g, b, a)
		if r == self.prev_pal[1][1] and g == self.prev_pal[1][2] and b == self.prev_pal[1][3] then
			return self.pal.tbl[n][1][1], self.pal.tbl[n][1][2], self.pal.tbl[n][1][3], a
		elseif r == self.prev_pal[2][1] and g == self.prev_pal[2][2] and b == self.prev_pal[2][3] then
			return self.pal.tbl[n][2][1], self.pal.tbl[n][2][2], self.pal.tbl[n][2][3], 1
		elseif r == self.prev_pal[3][1] and g == self.prev_pal[3][2] and b == self.prev_pal[3][3] then
			return self.pal.tbl[n][3][1], self.pal.tbl[n][3][2], self.pal.tbl[n][3][3], 1
		elseif r == self.prev_pal[4][1] and g == self.prev_pal[4][2] and b == self.prev_pal[4][3] then
			return self.pal.tbl[n][4][1], self.pal.tbl[n][4][2], self.pal.tbl[n][4][3], 1
		else
			return r, g, b, a
		end
	end)
	self.image:replacePixels(self.data)
	self.pal.current_pal = n
end

-- i think getColor is pretty self explanatory
function Tubit:getColor(n)
	assert(n <= 4 and n >= 1, "getCol() must be between 1 and 4!")
	return self.pal.tbl[self.pal.current_pal][n]
end

function Tubit:getSprite(x, y, w, h)
	return Sprite.new(self.image, x, y, w, h)
end

function Tubit:newAnimation(sw, sh, pw, ph)
	return Animation.new(self.image, sw, sh, pw, ph)
end

function Tubit:updatePaletteSwapper(dt)
	if self.input:pressed "left" then
		local newpal = 0
		if self.pal.current_pal == 1 then newpal = #self.pal.tbl
		else newpal = self.pal.current_pal - 1 end
		self:setPal(newpal)

	elseif self.input:pressed "right" then
		local newpal = 0
		if self.pal.current_pal == #self.pal.tbl then newpal = 1
		else newpal = self.pal.current_pal + 1 end
		self:setPal(newpal)
	end
end

function Tubit:togglePaletteSwapper() self.pal:togglePaletteSwapper() end
function Tubit:isPaletteSwapperActive() return self.pal:isPaletteSwapperActive() end
function Tubit:drawPaletteSwapper() self.pal:drawPaletteSwapper() end

function Tubit.render(fn)
	TLfres.beginRendering(160, 144)
	fn()
	TLfres.endRendering()
end

-- draw a basic 16x16 (or whatever the tilesize is) sprite
function Tubit:draw(n, x, y)
	love.graphics.setColor(1, 1, 1, 1)
	love.graphics.draw(self.image, self.quads[n], x, y)
end

return Tubit