local TubitPal = {}
local TubitPal_mt = { __index = TubitPal }

-- convert colors from hex to 1 based numbers love2d uses
local function hex2rgb(hex)
    hex = hex:gsub("#","")
    return tonumber("0x"..hex:sub(1,2)), tonumber("0x"..hex:sub(3,4)), tonumber("0x"..hex:sub(5,6))
end

function TubitPal.get_color(hex)
    local r,g,b = hex2rgb(hex)
    return { r/255, g/255, b/255 }
end

function TubitPal.new(tbl, tpath)
	assert(tbl ~= nil and type(tbl) == "table", "new palette requires a table of colors")

	-- we need to convert all the colors before saving
	for pal=1, #tbl do
		for col=1, 4 do
			tbl[pal][col] = TubitPal.get_color(tbl[pal][col])
		end
	end

	return setmetatable({
		current_pal = 1,
		tbl = tbl,
		theme = { active = false }
	}, TubitPal_mt)
end

function TubitPal:print(str, x, y, c)
	local r, g, b, a = love.graphics.getColor()
	love.graphics.setColor(self.tbl[self.current_pal][c])
	love.graphics.print(str, x, y)
	love.graphics.setColor(r, g, b, a)
end

-- returns the beautiful purple haze palette. totally not biased.
-- also we begin with the default r, g, b values
function TubitPal.getDefaultPalette()
	return {
		{'000000', 'ff0000', '00ff00', '0000ff' },
		{'4a247c', '937ac5', 'c1b2e1', 'dbcef5' }
	}
end

function TubitPal:togglePaletteSwapper()
	self.theme.active = not self.theme.active and true or false
end

function TubitPal:isPaletteSwapperActive() return self.theme.active end

function TubitPal:drawPaletteSwapper()
	if self.theme.active then
		local x, y = 8, 48
		-- draw the background
		love.graphics.setColor(self.tbl[self.current_pal][1])
		love.graphics.rectangle("fill", 4, 16, 152, 120, 4, 4)

		self:print("Select Theme", 32, 24, 3)


		for p=1, #self.tbl do
			if self.current_pal == p then
				love.graphics.setColor(self.tbl[p][4])
				love.graphics.rectangle("line", (x+8)-1, y-1, 34, 10)
			end
			for c=1, #self.tbl[p] do
				love.graphics.setColor(self.tbl[p][c])
				love.graphics.rectangle("fill", x+(c*8), y, 8, 8)
				if c % 4 == 0 then x = x+34 end
			end

			if p % 4 == 0 then
				x = 8
				y = y + 16
			end
		end
	end
end

return TubitPal