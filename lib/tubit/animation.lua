local TubitAnim = {}
local TubitAnim_mt = { __index = TubitAnim }

function TubitAnim.new(sheet, sx, sy, pw, ph)

	return setmetatable({
		sheet = sheet,
		quads = {},
		viewport = { x = sx, y = sy },
		states = {},
		state = false,
		flip = false,
		offset = ph ~= nil and { x = (sx - pw) / 2, y = sy - ph } or { x = 0, y = 0 },
		dim = ph ~= nil and { w = pw, h = ph } or false
	}, TubitAnim_mt)
end

function TubitAnim:getStateCount()
	local c = 0
	for _,_ in pairs(self.states) do c = c + 1 end
	return c
end

function TubitAnim:addAnimation(name, frames, fps, cb)
	self.states[name] = {
		name = name,
		frames = frames,
		fps = fps or 0,
		cb = cb or false,
		quads = {},
		paused = false,
		rotate = false,
		rotate_deg = 0,
		pos = 1,
		t = 0,
	}

	local sw, sh = self.viewport.x, self.viewport.y
	for i = 1, #frames do
        self.states[name].quads[i] = love.graphics.newQuad(frames[i][1], frames[i][2], sw, sh, self.sheet:getDimensions())
    end

    -- if it's the first entry, set the current state to this one
    if self:getStateCount() <= 1 then self.state = name end

    return self
end

function TubitAnim:setFlip(b)
	self.flip = b
end

function TubitAnim:setRotate(state)
    self.states[state].rotate = true
end

function TubitAnim:setState(state)
    assert(self.states[state], "No such animation state exists: " .. state)

    local cs = self.states[self.state]
    --if not cs then
    --    self.state = self.states[1].name

    if cs.name ~= state then
        cs.pos = 1
        cs.paused = false
        if cs.rotate then cs.rotate_deg = 0 end
    end

    self.state = state
end

function TubitAnim:update(dt)
	local state = self.states[self.state]
	if state.fps > 0 and not state.paused then
        state.t = state.t - dt
        if state.t <= 0 then
            if state.rotate then
                if self.flip then
                    state.rotate_deg = state.rotate_deg-30
                else
                    state.rotate_deg = state.rotate_deg+30
                end
                if state.rotate_deg >= 360 then state.rotate_deg = 0 end
            end

            state.pos = state.pos + 1
            state.t = 1 / state.fps
            if state.pos > #state.quads then
                if state.cb then
                    local cb = state.cb
                    if type(cb) == "string" then self.ply[cb](self.ply, self)
                    else state.cb(self) end
                end
                if not state.paused then state.pos = 1 else state.pos = #state.quads end
            end
        end
    end
end

function TubitAnim:getOffsets(w, h)
	w = (self.viewport.x - w) / 2
	h = self.viewport.y - h
	return { x = w, y = h }
end

function TubitAnim:rotatePoint(x, y, angle)
	local xrot = math.cos(angle) * x - math.sin(angle) * y
	local yrot = math.sin(angle) * x + math.cos(angle) * y
	return xrot, yrot
end

function TubitAnim:draw(x, y)
	local rot = 0
	local state = self.states[self.state]
	local xoff, yoff = (self.flip and self.offset.x ~= 0 and self.dim) and self.offset.x+self.dim.w or self.offset.x,
                        self.offset.y and self.offset.y or 0

    if state.rotate then
        rot = math.rad(state.rotate_deg)
        local sw, sh = self.viewport.x, self.viewport.y
        xoff = self.flip and (sw / 2) or (sw+xoff) / 2
        yoff = (sh+yoff) / 2
    end

	love.graphics.draw(
		self.sheet,
		state.quads[state.pos],
		state.rotate and x+self.dim.w/2 or x,
		state.rotate and y+self.dim.h/2 or y,
		rot,
		self.flip and -1 or 1, -- flip h
        1, -- flip v
        xoff, -- xoffset
        yoff -- yoffset
	)
end

return TubitAnim