local tubit = require "lib.tubit"

local drawSwapper = false

-- new(spritesheet_path, default_filter?)
local sheet = tubit.new("sprites.png", true)

-- our player object
local player = {
	x = 61,
	y = 102-17,
	w = 14,
	h = 17
}

-- newAnimation(sprite width, sprite height)
-- if your bounding box size is different from the sprite size (in most cases will be)
-- pass the width and height, and tubit will try to work out the correct offsets for you
player.anim = sheet:newAnimation(32, 32, player.w, player.h)

-- addAnimation(state name/identifier, table { quadx, quady }, fps, callback)
player.anim:addAnimation("idle", {
	{ 0, 64 }, { 32, 64 }, { 64, 64 }, { 96, 64 }, { 128, 64 }
}, 8)

player.anim:addAnimation("run", {
	{ 0, 96 }, { 32, 96 }, { 64, 96 }, { 96, 96 }, { 128, 96 }, { 0, 128 }
}, 14)

player.anim:addAnimation("weee", {
	{ 0, 96 }
}, 32)

player.anim:setRotate("weee")

-- sets the current state. if not set, it'll default to the first one that was created
player.anim:setState("idle")


local platform = sheet:getSprite(16, 0, 48, 48)

-- you can chain flipH(), flipV() onto getSprite() if you want to
--local spr = sheet:getSprite(0, 64, 32, 32):flipH(true)

-- adding a new palette is pretty easy.
-- here's some from lospec
-- icecream GB
sheet:addPal({ '7c3f58', 'eb6b6f', 'f9a875', 'fff6d3' })
-- nintendo gameboy BGB
sheet:addPal({ '081820', '346856', '88c070', 'e0f8d0' })
-- mist GB
sheet:addPal({ '2d1b00', '1e606e', '5ab9a8', 'c4f0c2' })

-- replaces all the colors in the spritesheet
-- the first two palettes are reserved for defaults (RGB, and Purple Haze)
-- change the 3 to a 4 to enable the BGB palette!
sheet:setPal(3)

function love.update(dt)
	tubit.input:update(dt)

	if tubit.input:pressed "select" then sheet:togglePaletteSwapper() end

	if sheet:isPaletteSwapperActive() then
		sheet:updatePaletteSwapper(dt)
	else
		player.anim:update(dt)

		if tubit.input:down "left" then
			player.x = player.x - 20 * dt
			player.anim:setFlip(true)
			player.anim:setState("run")
		elseif tubit.input:down "right" then
			player.x = player.x + 20 * dt
			player.anim:setFlip(false)
			player.anim:setState("run")
		elseif tubit.input:down "a" then
			player.anim:setState("weee")
		else
			player.anim:setState("idle")
		end
	end
end

function drawBoundingBox()
	love.graphics.setColor(1, 1, 1)
	love.graphics.rectangle("line", player.x, player.y, player.w, player.h)
end

function love.draw()
	tubit.render(function()
		love.graphics.clear(sheet:getColor(4))

		-- we can do basic printing in the gameboy style too!
		-- string, x, y, color
		sheet:print("Look ma', it's Tubit", 8, 40, 1)

		-- draw user-defined sprite quad
		platform:draw(60, 102)

		-- draw animation
		player.anim:draw(player.x, player.y)

		-- just to show where our bounding box is
		--drawBoundingBox()

		-- draw a single quad from the main sheet
		-- sprite number, x, y
		sheet:draw(25, 76, 118)

		sheet:drawPaletteSwapper()
	end)
end