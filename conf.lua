function love.conf(t)
    t.window.width = 160*4
    t.window.height = 144*4
    t.window.vsync = false
    t.modules.physics = false
    t.window.fullscreen = false
end
