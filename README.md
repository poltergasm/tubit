# Tubit
### the 2bit palette swapper for LÖVE

![](tubitdemo.gif)

## Huh?
This library is intended for somewhat mimicking gameboy graphics. You give it a spritehseet, and then you can 
swap the palette on the fly. It works by remapping the RGB values to the new palette you give it.
As a bonus, there's some tools for drawing quads and creating sprites (for larger ones that exceed the 16x16 tile size).

## Installation

First, grab a copy of luarocks.. HAHA JOKING. Simply copy the `tubit` folder into your project and require as normal!
For example, if you copied it to `yourproject/lib` then you'd use `local tubit = require "lib.tubit"`. If it's in your folder where `main.lua` resides, then `local tubit = require "tubit"`

## Usage

Load the library. Duh.

```lua
local tubit = require "lib.tubit"
```
Create a new sheet. just pass it the path to your spritesheet, and it'll do the rest. If you pass true as the second argument 
then it will set up the default filtering for pixel art (nearest). If not, you're welcome to set your own.

```lua
local sheet = tubit.new("sprites.png", true)
```

You'll probably want to set up some palettes. By default there's already 2 available. The basic RGB one (FF0000, 00FF00, 0000FF), and one I made up of purple shades (Purple Haze).
So, when you add a new palette, it will begin at 3.
Palettes are just made up of a table with 4 hex colors. Don't worry, they are automatically converted into values love2d can read. It helps if you keep the same format as you designed your spritesheet. I like to go darkest to lightest.

```lua
-- icecream GB and nintendo gameboy (bgb) from lospec
sheet:addPal({ '7c3f58', 'eb6b6f', 'f9a875', 'fff6d3' })
sheet:addPal({ '081820', '346856', '88c070', 'e0f8d0' })
```

If you don't set a palette, it will default to the ugly RGB one. You just need to call `sheet:setPal(n)` where `n` is the palette number. In the above example, icecream GB would be 3 and NGB BGB one would be 4. Let's use that BGB one for nostalgia reasons.

```lua
sheet:setPal(4)
```

Now, we can start drawing. You can use `sheet:getColor(n)` to retrieve a color in the current palette. For example, if we wanted to clear the screen with the lightest color from the palettes above.

```lua
function love.draw()
	love.graphics.clear(sheet:getColor(4))
end
```

Currently there's three ways to draw a sprite.

1. Draw a single tile from the spritesheet
2. Create a sprite from a quad (useful if your sprite is larger than 16x16)
3. An animation (see the Animation section for more information)

To draw a tile from the spritesheet, we just call `sheet:draw(sprite number, x, y)`. During the construction process, the image is broken up into 16x16 squares (quads). So you can just give the draw method the number of the sprite you want to draw, and voila!

```lua
function love.draw()
	sheet:draw(2, 24, 16)
end
```

The above will draw the second tile and 24 across, 16 down.

If you need something a little more advanced we can create a sprite like so

```lua
local spr = sheet:getSprite(0, 64, 32, 32)
```

These are just quad values, as in `x, y, w, h`. While there's not a lot of transformation functionality right now, you can flip them horizontally and vertically with `spr:flipH(true/false)` and `spr:flipV(true/false)`.
You can even chain this method onto the `getSprite` call, like

```lua
local spr = sheet:getSprite(0, 64, 32, 32):flipH(true)

-- if you want to clone it for some reason
-- cloned sprites will not have the transformations (ie: flip)
local spr2 = spr:clone()
```

To draw them, you just call `spr:draw(x, y)

## Animations

Tubit also has animation support, if you want to use it. You can use another animation library if you wish!
To start off, let's create a really basic player "object"

```lua
local player = {
	x = 60,
	y = 128,
	w = 14,
	h = 17
}
```

You can name the variables whatever you want - this is just an example. In most cases, your actual entity dimensions (width and height) will be different from its actual sprite size. This is known as a bounding box, but it can also make things tricky when it comes to drawing your sprite, because the sprite will need to be offset correctly so it doesn't jump around out of place. Tubit tries to handle that for you, so I won't go into too much detail.

Once we have our player object, we create an animation object for them to use.

```lua
player.anim = sheet:newAnimation(32, 32, player.w, player.h)
```

We passed the sprites width and height (32), and also the players width and height. Tubit will perform the offset calculation here. The last two arguments are completely optional, in which case the offset will always be zero, which might look weird. But hey, it's your game.

Now, let's add some animations so our player can actually do stuff.

```lua
player.anim:addAnimation("idle", {
	{ 0, 64 }, { 32, 64 }, { 64, 64 }, { 96, 64 }, { 128, 64 }
}, 8)
```

This looks scary, but it's not too bad. First we pass the animation an "identifier" or name. It can be whatever. Second, is a table of vectors, which are basically just the x and y coordinates on the spritesheet. Most drawing applications will have a way to retrieve these values.
Third argument is the fps. The higher the number, the faster the animation will play out. A final argument can be passed, which is a callback that runs once the animation has finished its cycle. This is useful if you want to break up animations into parts, for example, a mighty swing of a hammer, but you only want the damage to occur on its ways down.

```lua
player.anim:addAnimation("swing", {
	{ 0, 24 }, { 24, 24 }, 8, function(a) a:setState("hit") end)
}, 8)

player.anim:addAnimation("hit", {
	{ 48, 24 }, { 72, 24 }, 14, function() self:hulk_smash() end)
}, 8)
```

The callback takes an argument, which is just a shortcut to the animation object. You're totally welcome.

By default, it will use the first created animation as its state, but you can set it at any time with `setState()`

```lua
player.anim:setState("picknose")
```

Another important feature is being able to flip a sprite (ie: turning left and right). To do that, just call `setFlip`.

```lua
player.anim:setFlip(true)
player.anim:setFlip(false)
```

More cool stuff we can do is tell tubit to rotate an animation whenever it plays. Could be used for weapons that need to spin, like throwing hammers/knives, or death animations (like Mario style).
Simply call `player.anim:setRotate(state_name)` and it'll flag it to rotate when it's active.
The speed of the rotation is based on the fps you set on the animation. It's also able to flip like any other animation.

```lua
player.anim:setRotate("stomped")
```

Of course, if we don't update the animation, then the frames will never progress and you'll just see the first frame. To do that simply call `:update(dt)`

```lua
function love.update(dt)
	player.anim:update(dt)
end
```

Finally, we draw it to the screen

```lua
function love.draw()
	player.anim:draw(player.x, player.y)
end
```

The draw method will handle all the flipping and offsets for you, so you can just kick back, sip some tea and watch your awesome sprites moving about.

## Rendering and letter box

Tubit can use TLfres to scale your games resoltion to 160x144 no matter what screen size you're using. This means you don't have to worry about scaling things yourself, and you can just draw things as you normally would.
Simply wrap all your draw code in `tubit.render(function() ... end)`
As usual, this is completely optional and you don't have to use it.

```lua
function love.draw()
	tubit.render(function()
		love.graphics.clear(sheet:getColor(4))
		sheet:print("Hello, World!", 24, 24, 1)
	end)
end
```

## Input

Tubit includes Baton, an input handler by tesselode. One of the best in my personal opinion. Once you load Tubit, it becomes global, because you'll probably use it everywhere. It can be accessed via `tubit.input`.
The current mappings for keyboard/controller look like this

 - Left (left / dpad left)
 - Right (right / dpad right)
 - Up (up / dpad up)
 - Down (down / dpad down)
 - Start (enter / start button)
 - Select (shift / back button)
 - A (x / A button)
 - B (z / X button)

Don't worry, it will all be customisable in a later release. Some examples.

Before using the input library, you should update it to check for events.

```lua

local tubit = require "lib.tubit"

function love.update(dt)
	tubit.input:update(dt)

	if tubit:pressed "a" then player:jump() end
	if tubit:pressed "b" then player:shoot() end
	if tubit:down "right" then player:walk_right() end
	else tubit:down "left" then player:walk_left() end
	if tubit:pressed "start" then game:pause() end
end
```

Baton is an extremely easy library to use. If you want to learn more, check out https://github.com/tesselode/baton

## Theme swapper dialog

Also included, is a nifty little dialog for changing themes on the fly. It's completely optional, but you could add it into you game to give the user more options depending on their personal preferences.

All you need to do is to toggle it, update it, and draw it. Tubit will handle the rest.

```lua

function love.update(dt)
	tubit.input:update(dt)

	if tubit.input:pressed "select" then sheet:togglePaletteSwapper() end

	-- if it's active, then pause the game and update the swapper dialog
	if sheet:isPaletteSwapperActive() then
		sheet:updatePaletteSwapper(dt)
	else
	    game:update(dt)
	end
end

function love.draw()
	game:draw()

	-- it will only be drawn when active, so it's safe to 
	-- not wrap it in a conditional here
	sheet:drawPaletteSwapper()
end
```

In the above example, when the user presses the select button, the palette swapper will toggle and allow them to navigate the palletes using the right and left buttons. Once they press select again, it will toggle off and return them to the game.